#include <stdint.h>
#include "nrfx_twim.h"
#include "nrfx_gpiote.h"
#include "mouseCtrl.h"
#include "imu.h"

static void initI2c(void);
static void btnInit(void);
static void gpioteEvtHandler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action);


static const uint32_t PinI2cSda = 23;
static const uint32_t PinI2cScl = 22;
static const nrfx_twim_t twim = NRFX_TWIM_INSTANCE(1);
static const uint32_t BtnPins[4] = {13,14,15,16};

static bool leftButton = false;
static bool rightButton = false;
static bool middleButton = false;

void mouseCtrlInit(void)
{
    btnInit();
    initI2c();
    imuInit();
}

void mouseCtrlGetAxis(int8_t *x, int8_t *y)
{
    int8_t ax, ay, az;
    imuReadG(&ax, &ay, &az);
    *x = ax;
    *y = ay;
}

int8_t mouseCtrlGetPan(void)
{
    return 0;
}

int8_t mouseCtrlGetScroll(void)
{
    return 0;
}

uint8_t mouseGetButtons(void)
{
    uint8_t btn = 0;
    btn |= leftButton ? 0x01 : 0x00;
    btn |= rightButton ? 0x02 : 0x00;
    btn |= middleButton ? 0x04 : 0x00;
    return btn;
}

static void initI2c(void)
{
    nrfx_twim_config_t config = {
        .scl = PinI2cScl,
        .sda = PinI2cSda,
        .frequency = NRF_TWIM_FREQ_400K,
        .interrupt_priority = NRFX_TWIM_DEFAULT_CONFIG_IRQ_PRIORITY,
        .hold_bus_uninit = NRFX_TWIM_DEFAULT_CONFIG_HOLD_BUS_UNINIT
    };

    nrfx_err_t ret = nrfx_twim_init(&twim, &config, NULL, NULL);
    nrfx_twim_enable(&twim);
}


void imuDataWrite(const uint8_t *data, uint32_t length)
{
    nrfx_twim_xfer_desc_t xfer = NRFX_TWIM_XFER_DESC_TX(IMU_ADDRESS, data, length);
    nrfx_err_t err = nrfx_twim_xfer(&twim, &xfer, 0);
}

void imuDataRead(uint8_t reg, uint8_t *rxData, uint32_t rxLength)
{
    nrfx_twim_xfer_desc_t xfer = NRFX_TWIM_XFER_DESC_TXRX(IMU_ADDRESS, &reg, sizeof(reg), rxData, rxLength);
    nrfx_err_t err = nrfx_twim_xfer(&twim, &xfer, 0);
}

static void btnInit(void)
{
    nrfx_gpiote_init();
    for (uint32_t i = 0; i < 4; i++) {
            const nrfx_gpiote_in_config_t cfg = {
                .sense = NRF_GPIOTE_POLARITY_TOGGLE,
                .pull = NRF_GPIO_PIN_PULLUP,
                .is_watcher = false,
                .hi_accuracy = true,
                .skip_gpio_setup = false,
            };
            nrfx_gpiote_in_init(BtnPins[i], &cfg, gpioteEvtHandler);
            nrfx_gpiote_in_event_enable(BtnPins[i], true);
    }
}

static void gpioteEvtHandler(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    bool state = nrfx_gpiote_in_is_set(pin);
    switch(pin) {
    case 13:
        leftButton = !state;
        break;
    case 14:
        middleButton = !state;
        break;
    case 15:
        rightButton = !state;
        break;
    case 16:
        break;
    default:
        break;
    }
}
