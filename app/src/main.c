#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nrf_delay.h"
#include "mouseCtrl.h"
#include "mouseSvc.h"
#include "app_timer.h"
#include "nrfx_clock.h"
#include "nrf_pwr_mgmt.h"

static bool readMouse = false;
static volatile bool lfClkStarted = false;

APP_TIMER_DEF(mouseTimer);

static void clockEvtHandler(nrfx_clock_evt_type_t event);
static void timerTimeoutHandler(void *ctx);

int main(void)
{
    // only if softdevice is not used, otherwise remove nrfx_clock module

    /*
    nrfx_clock_init(clockEvtHandler);
    nrfx_clock_enable();
    nrfx_clock_lfclk_start();
    while(!lfClkStarted) {}
    */


    mouseCtrlInit();
    ret_code_t ret = nrf_pwr_mgmt_init();
    ret = app_timer_init();
    ret = app_timer_create(&mouseTimer, APP_TIMER_MODE_REPEATED, timerTimeoutHandler);
    mouseSvcInit();



    ret = app_timer_start(mouseTimer, APP_TIMER_TICKS(10), NULL);
    while (true)
    {
        if (readMouse) {
            readMouse = false;
            MouseSvcData mouseData;
            mouseCtrlGetAxis(&mouseData.x, &mouseData.y);
            mouseData.pan = mouseCtrlGetPan();
            mouseData.scroll = mouseCtrlGetScroll();
            mouseData.buttons = mouseGetButtons();
            mouseSvcSend(&mouseData);
        } else {
            nrf_pwr_mgmt_run();
        }
    }
}

static void timerTimeoutHandler(void *ctx)
{
    (void)ctx;
    readMouse = true;
}

static void clockEvtHandler(nrfx_clock_evt_type_t event)
{
    switch(event) {
        case NRFX_CLOCK_EVT_LFCLK_STARTED:
            lfClkStarted = true;
            break;
    }
}
