#pragma once

#include <stdint.h>

void mouseCtrlInit(void);
void mouseCtrlGetAxis(int8_t *x, int8_t *y);
int8_t mouseCtrlGetPan(void);
int8_t mouseCtrlGetScroll(void);
uint8_t mouseGetButtons(void);
