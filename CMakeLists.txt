cmake_minimum_required(VERSION 3.15)
include(cmake/toolchain.cmake)
project(BleMouse C ASM)

add_compile_options(
    -mabi=aapcs
    -mthumb
    -fdata-sections
    -ffunction-sections
    -g3
    -mcpu=cortex-m4
    -mfloat-abi=soft
    -fshort-enums
    -O0
)

add_link_options(
    -mthumb
    -mfloat-abi=soft
    -mcpu=cortex-m4
    -Wl,-gc-sections
    -fno-strict-aliasing
    -g3
    -Wl,--no-wchar-size-warning
    -Wl,-gc-sections
    -fno-builtin
    -fshort-enums
)

add_subdirectory(sdk)
add_subdirectory(app)
